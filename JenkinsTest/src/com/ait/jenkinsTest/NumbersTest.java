package com.ait.jenkinsTest;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ait.jenkins.Numbers;

public class NumbersTest {

	@Test
	public void test() {
		assertTrue(Numbers.isItSmaller(1, 2));
		assertFalse(Numbers.isItSmaller(2, 1));
	}

}
